/*
Google map API Facade/Service 

*/


export module MapUtil {

    let _map: any;
    let _mapGoogleApi: any;
    let _markers: any[] = [null, null];
    let _directionsService:any;
    let _directionsDisplay: any;
    let _event: Object = {};
    
    export function setGoogleMapApi(googleMapApi: any) { 
        _mapGoogleApi = googleMapApi; // Set the Google API Library
    }

    export function init(element: string, config: any = {}): {} { 
        let setConfig = Object.assign({}, { // Init the Map
            center: {
                lat: 40.44,
                lng: -97.03125
            },
            zoom: 4,
            fullscreenControl: false,
            keyboardShortcuts: false,
            mapTypeControl: false,
            streetViewControl: false,
            zoomControl: false
        }, config );
        
        try {
            if (!_mapGoogleApi) { throw 'Need to set the google API using setMapApi'; }
            // Check if the Library is present before reate map
            _map = new _mapGoogleApi.Map(document.getElementById(element), { ...setConfig });
            _directionsService = new _mapGoogleApi.DirectionsService; // API for pre rendering Route
            _directionsDisplay = new _mapGoogleApi.DirectionsRenderer; // API for rendering
            _directionsDisplay.setMap(_map); // set Layer for route

        } catch (error) {
            console.error(error);
        }
        
        return _map;
    }

    export function Map(): {} { 
        return _map; // Return the MAP
    }

    export function GoogleMapApi() { 
        return _mapGoogleApi; // Return the Google API Library
    }

    export function addMarker(coordinate: any, index: number) { // Add Marker
        if (coordinate) {
            if (_markers[index]) { 
                removeMarker(index);
                _markers[index] = null;
            }
            
            _markers[index] = new _mapGoogleApi.Marker({
                position: new _mapGoogleApi.LatLng(coordinate.lat, coordinate.lng),
                map: _map,
                title: 'AIRPORT'
            });

            makeRoute();  
        }
    }

    export function removeMarker(index: number) {  // Remove Marker
        if (_markers[index]) { 
            _markers[index].setMap(null);
            _markers[index] = null;
        }
    }

    export function getMarker(index: number) {  
        return _markers[index]; // Get Marker
    }




    // Event Creator Pattern this could be in other file.
    export function notifyEvent(event: string, data: any): void{ 
        if (_event.hasOwnProperty(event)) {
            for (let xf = 0; xf < _event[event].length; xf++) { 
                _event[event][xf](data);
            }
        }
    }

    export function listingEvent(event: string, cb: Function): void { 
        if (!_event.hasOwnProperty(event)) { 
            _event[event] = [];    
        }
        
        _event[event].push(cb);
    }

    // Generate the route
    export function makeRoute() {
        if (_markers.every((marker) => marker !== null)) { // Confirm that both from and to are present in the menu

            let origin = new _mapGoogleApi.LatLng(_markers[0].position.lat(), _markers[0].position.lng());
            // Get the Lat, Lng from the marker
            let destination = new _mapGoogleApi.LatLng(_markers[1].position.lat(), _markers[1].position.lng());

            _markers[0].setMap(null);
            _markers[1].setMap(null);

            _directionsService.route(
                {
                    origin: origin,
                    destination: destination,
                    travelMode: 'DRIVING'
                },
                function (response: any, status: any) {
                    if (status === _mapGoogleApi.DirectionsStatus.OK) {
                      _directionsDisplay.setDirections(response);
                    } else {
                      window.alert('Directions request failed due to ' + status);
                    }
                }
            );
            
            // Notify  to menu that the route is complete
            MapUtil.notifyEvent('COMPLETE',
                                (_mapGoogleApi.geometry.spherical
                    .computeDistanceBetween(origin, destination) / 1000 * 0.539957)
                    .toFixed(2));
                    /* 
                    Google API to get the Distance from point A to B return in Meters converter to Kilomenter 
                    1000 meters / 1 KM / 0.539957 NM
                    */
        }

    }
}