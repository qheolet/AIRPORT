import './menu.css';

import * as React from 'react';
import * as airport from './airport';

import { AirportI, getCoordinate } from './airport';

import { MapUtil } from './mapUtil';
import SelectComponent from './select';

interface MenuComponentProps {    
}

interface MenuComponentState {
    nauticalMiles: number;
}

class MenuComponent extends React.Component<MenuComponentProps, MenuComponentState> {

    constructor(props: MenuComponentProps) {
        super(props);
        this.state = { nauticalMiles: 0 };
    }

    componentDidMount() { 
        MapUtil.listingEvent('COMPLETE', this.setNauticalMiles.bind(this));
    }
    
    setNauticalMiles(nauticalMiles: number) {
        this.setState({ nauticalMiles });
    }

    getCoordinateLocal(iata: string, index: number) {
        MapUtil.addMarker(getCoordinate(iata), index);   
    }

    public render(): JSX.Element {
        // Filter the Array of Airports by Alphabet
        let listAirPorts: AirportI[] = airport.getAllByCountry('United States')
            .map((a: AirportI) => {
                return a;
            }).sort((a, b) => {
                if (a.name.toLowerCase() > b.name.toLowerCase()) { 
                    return 1;
                }

                if (a.name.toLowerCase() < b.name.toLowerCase()) { 
                    return -1;
                }
                
                return 0;

            });
        
        return (
            <div id="menu" className="menu">
                <div className="__title">
                    <h3>AIRPORT MAP</h3>
                </div>
                <div className="__dropdown" >
                    <table style={{width: '100%'}}>
                        <tbody>
                            <tr>
                                <td>
                                    <p className="NauticalMiles">
                                        {this.state.nauticalMiles > 0
                                            ? this.state.nauticalMiles + ' Nautical Miles'
                                            : null}
                                    </p>
                                </td>
                                <td>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <span>From:
                                                    </span>
                                                </td>
                                                <td><SelectComponent airports={listAirPorts} index={0}/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>
                                                        To:
                                                    </span>
                                                </td>
                                                <td><SelectComponent airports={listAirPorts} index={1}/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
            </div>    
        );
    }
}

export default MenuComponent;