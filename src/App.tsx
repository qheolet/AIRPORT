import './App.css';

import * as React from 'react';

import  MapComponent from './map';
import  MenuComponent  from './menu';

class App extends React.Component {
  render() {
    return (
      <div>
        <MenuComponent />
        <MapComponent />
      </div>  
    );
  }
}

export default App;
