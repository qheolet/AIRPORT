import './select.css'

import * as React from 'react';

import { AirportI, getCoordinate } from './airport';

import { MapUtil } from './mapUtil';
import { MouseEvent } from 'react';

interface SelectComponentProps {
    airports: AirportI[];
    // callback: Function;
    index: number;
}

interface SelectComponentState {
    airports: AirportI[];
    input: string;
}

class SelectComponent extends React.Component<SelectComponentProps, SelectComponentState> {

    constructor(props: SelectComponentProps) {
        super(props);
    
        this.state = {
            airports: [],
            input: ''
        };
    }

    // Click the Dropdown list
    clickList( name: string, iata: string) { 
        this.setState({
            airports: [],
            input: name + ' - ' + iata
        });

        MapUtil.addMarker(getCoordinate(iata), this.props.index);
    }

    // clean input field when you enter the input box
    cleanField(e: any) { 
        this.setState({
            input: ''
        });

        MapUtil.removeMarker(this.props.index);

    }

    onchange(e: any) { 
        this.setState({
            input: e.target.value
        });

        var value: string = e.target.value.toLowerCase();

        let found = this.props.airports // Filter Airport by input criteria base name and Iata code
            .filter((a: AirportI) => {            
                function check(p: string) {
                    return (p !== null) ? p.toLowerCase().indexOf(value) >= 0 : false;
                }

                return check(a.iata)  || check(a.name);
            }
        );

        this.setState({
            airports: found 
        });
    }

    public render(): JSX.Element {
        let options = this.state.airports.map((s: AirportI) => {
            return (
                <li key={s.iata + '-' + s.icao} onClick={(e: MouseEvent<HTMLElement>) => this.clickList(s.name, s.iata)}>
                    {s.name + ' - ' + s.iata}
                </li>);
        });
        
        return (
            <div className="searchBox">
                <input ref={() => 'input'} onFocus={(e) => this.cleanField(e) } onChange={(e) => this.onchange(e)} value={this.state.input} />
                {(options.length > 0) ? <ul>  {options} </ul> : null}
            </div>
        );

    }
}

export default SelectComponent;
