export interface AirportI { // Interface
    id: number;
    name: string;
    city: string;
    iata: string;
    country: string;
    latitude: number;
    longitude: number;
    altitude: number;
    icao: string;
}

export interface CoordanateI { // Interface
    lng: number;
    lat: number;
}

export const fetchAirportData = require('airport-data/airports.json'); // Import JSON FILE

export function getAllByCountry(country: string): AirportI[] { // Get Airport Buy Country
    return fetchAirportData.filter((airport: AirportI) => {
        return airport.country === country;
    });
}

export function getCoordinate(iataCode: string): CoordanateI | boolean { // Get Lat lng from Iata code
    
    const airport: AirportI[] = fetchAirportData.filter((airPort: AirportI) => iataCode === airPort.iata);

    try {
        if (!airport.length) {
            throw 'Iata code does not exist';
        }
    } catch (error) {
        console.log(error);
        return false;
    }
    return {
        lng: airport[0].longitude,
        lat: airport[0].latitude
    };
}