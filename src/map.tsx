import './map.css';

import * as React from 'react';

import { CONFIG } from './config';
import { MapUtil } from './mapUtil';

var mapsapi = require( 'google-maps-api' )( CONFIG.KEY );
 
interface MapComponentProps {};

interface MapComponentState {};

class MapComponent extends React.Component<MapComponentProps, MapComponentState> {

    setMapSize() { // set the Size of the HTML Structure
        const mapElement: HTMLElement | null  = document.getElementById('map');
        const menu: HTMLElement | null = document.getElementById('menu');
        const h =  menu ? menu.clientHeight : 0;
        const height = window.innerHeight - h;
        const width = window.innerWidth;

        if (mapElement) {
            mapElement.style.height = height + 'px';
            mapElement.style.width = width + 'px';
        }  
    }
    
    componentDidMount() { 
        this.setMapSize();
        window.addEventListener('resize', this.setMapSize); // Add Event Listerner when size resize.
 
        mapsapi().then( (googleMap: {}) => {
            MapUtil.setGoogleMapApi(googleMap); // Set Library map
            MapUtil.init('map'); // Init the map in the
        });
    }

    componentWillUnmount() { 
        window.removeEventListener('resize', this.setMapSize);    // Remove Event when component  Unmount
    }
    
    public render(): JSX.Element {
        function ref() { 
            return 'map';
        }

        return (
            <div id="map" ref={ref()} className="map" />
        );
    }


}

export default MapComponent;
